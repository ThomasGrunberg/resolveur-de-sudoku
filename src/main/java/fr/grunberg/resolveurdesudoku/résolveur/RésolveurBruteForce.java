package fr.grunberg.resolveurdesudoku.résolveur;

import fr.grunberg.resolveurdesudoku.structure.Case;
import fr.grunberg.resolveurdesudoku.structure.ExceptionSymboleInvalide;
import fr.grunberg.resolveurdesudoku.structure.Grille;
import fr.grunberg.resolveurdesudoku.structure.Symbole;

/**
 * Résoud le sudoku par la brute force
 */
public class RésolveurBruteForce extends Résolveur {
	private boolean debugEtapeParEtape = false;

	public RésolveurBruteForce(Grille grilleARésoudre) {
		super(grilleARésoudre);
	}

	@Override
	public boolean Résoudre() {
		démarrerRésolution();
		
		// Poursuivre tant qu'il existe une prochaine cellule vide
		int[] prochaineCelluleVide = grilleARésoudre.getProchaineCelluleVide();
		Symbole dernierSymboleUtilisé = Case.SymboleVide;
		while(prochaineCelluleVide != null) {
			Symbole prochainSymboleAUtiliser = dernierSymboleUtilisé;
			boolean prochainSymboleInvalide = false;
			try {
				prochainSymboleAUtiliser = grilleARésoudre.getProchainSymbole(prochainSymboleAUtiliser);
			}
			catch(ExceptionSymboleInvalide e) {
				prochainSymboleInvalide = true;
			}
			
			boolean changementFait = false;
			// Cas où il faut revenir en arrière (tous les symboles épuisés)
			if(prochainSymboleInvalide) {
				if(debugEtapeParEtape)
					System.out.println("On a rempli " + prochaineCelluleVide[0] + "-" + prochaineCelluleVide[1] + " avec un mauvais symbole (" + prochainSymboleAUtiliser + "), on est à court d'options, on recule.");
				grilleARésoudre.effacerSymbole(prochaineCelluleVide[0], prochaineCelluleVide[1]);
				prochaineCelluleVide = grilleARésoudre.getDernièreCelluleEcrite();
				if(prochaineCelluleVide == null)
					return terminerRésolution();
				dernierSymboleUtilisé = grilleARésoudre.getSymbole(prochaineCelluleVide[0], prochaineCelluleVide[1]);
				grilleARésoudre.effacerSymbole(prochaineCelluleVide[0], prochaineCelluleVide[1]);
				changementFait = true;
			}
			
			if(!changementFait) {
				grilleARésoudre.écrireSymbole(prochaineCelluleVide[0], prochaineCelluleVide[1], prochainSymboleAUtiliser);
				// Cas où le symbole choisi est correct, on peut avancer
				if(grilleARésoudre.isValide()) {
					if(debugEtapeParEtape)
						System.out.println("On a rempli " + prochaineCelluleVide[0] + "-" + prochaineCelluleVide[1] + " avec un bon symbole (" + prochainSymboleAUtiliser + "), on avance.");
					prochaineCelluleVide = grilleARésoudre.getProchaineCelluleVide();
					dernierSymboleUtilisé = Case.SymboleVide;
					changementFait = true;
				}
				// Cas où le symbole choisi est incorrect, il faut essayer un autre symbole
				else {
					if(debugEtapeParEtape)
						System.out.println("On a rempli " + prochaineCelluleVide[0] + "-" + prochaineCelluleVide[1] + " avec un mauvais symbole (" + prochainSymboleAUtiliser + "), on réessaie.");
					grilleARésoudre.effacerSymbole(prochaineCelluleVide[0], prochaineCelluleVide[1]);
					dernierSymboleUtilisé = prochainSymboleAUtiliser;
					changementFait = false;
				}
			}
			
			if(debugEtapeParEtape && changementFait)
				grilleARésoudre.afficherGrille();
			
		}
		
		return terminerRésolution();
	}

}
