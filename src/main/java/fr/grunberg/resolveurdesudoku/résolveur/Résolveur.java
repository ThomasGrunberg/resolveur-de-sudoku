package fr.grunberg.resolveurdesudoku.résolveur;

import fr.grunberg.resolveurdesudoku.structure.Grille;

/**
 * Classe pouvant résoudre une grille de sudoku
 */
public abstract class Résolveur {
	private long débutRésolution;
	private long tempsRésolution;
	
	protected Grille grilleARésoudre;
	
	public Résolveur(Grille grilleARésoudre) {
		this.grilleARésoudre = grilleARésoudre;
	}

	/**
	 * Résoud la grille
	 * @param grilleARésoudre
	 * @return
	 */
	public abstract boolean Résoudre();

	public long getTempsRésolution() {
		return tempsRésolution;
	}
	
	protected void démarrerRésolution() {
		débutRésolution = System.currentTimeMillis();
	}
	
	protected boolean terminerRésolution() {
		tempsRésolution = System.currentTimeMillis() - débutRésolution;
		return grilleARésoudre.isComplet() && grilleARésoudre.isValide();
	}
	
}
