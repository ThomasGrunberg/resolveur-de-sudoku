package fr.grunberg.resolveurdesudoku.structure;

public class ExceptionSymboleInvalide extends RuntimeException {
	private static final long serialVersionUID = 5415847417278855516L;

	public ExceptionSymboleInvalide(String msg) {
		super(msg);
	}
}
