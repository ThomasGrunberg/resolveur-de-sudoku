package fr.grunberg.resolveurdesudoku.structure;

public class ExceptionSymboleImmuable extends RuntimeException {
	private static final long serialVersionUID = 5415847417278855515L;

	public ExceptionSymboleImmuable(String msg) {
		super(msg);
	}
}
