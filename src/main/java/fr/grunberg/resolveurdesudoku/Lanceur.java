package fr.grunberg.resolveurdesudoku;

import fr.grunberg.resolveurdesudoku.résolveur.Résolveur;
import fr.grunberg.resolveurdesudoku.résolveur.RésolveurBruteForce;
import fr.grunberg.resolveurdesudoku.résolveur.RésolveurIntuitif;
import fr.grunberg.resolveurdesudoku.structure.Grille;

/**
 * Traitement qui lance la recherche
 */
public class Lanceur {

	public static void main(String[] args) {
		Grille grilleARésoudre = initialiserGrille();
		grilleARésoudre.afficherGrille();
		
		Résolveur résolveur = new RésolveurBruteForce(grilleARésoudre);
		résolveur.Résoudre();
		grilleARésoudre.afficherGrille();
		System.out.println("Résolution en " + résolveur.getTempsRésolution() + "ms par RésolveurBruteForce");
		
		grilleARésoudre = initialiserGrille(); 

		résolveur = new RésolveurIntuitif(grilleARésoudre);
		résolveur.Résoudre();
		grilleARésoudre.afficherGrille();
		System.out.println("Résolution en " + résolveur.getTempsRésolution() + "ms par RésolveurIntuitif");
	}
	
	private static Grille initialiserGrille() {
		Grille grilleARésoudre = new Grille();
		grilleARésoudre.initialiserMethodeComplète(Grille.GrilleFacile1);
		//grilleARésoudre.initialiserRésultatAttendu("534678912672195348198342567859761423426853791713924856961537284287419635345286179");
		return grilleARésoudre;
	}
}
