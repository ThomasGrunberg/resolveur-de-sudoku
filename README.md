# Résolveur de sudoku

## Etape 1 : modélisation de sudoku
- Construire la grille
- Vérifier la validité de la construction
- Construire les indices ; passage d'arguments en ligne de commande
- Représentation (simple) de la grille
- Vérifier la validité de la grille à un moment donné

## Etape 2 : résolution de sudoku par brute force
- Modéliser les indices et la recherche de la solution
- Création d'un résolveur brute force et d'une classe abstraite de résolveur
- Mise en place de la recherche de la solution

## Etape 3 : résolution de sudoku par méthode intuitive/humaine
- Modéliser les indices et la recherche de la solution
- Création d'un résolveur "intuitif"
- Mise en place de la recherche de la solution
