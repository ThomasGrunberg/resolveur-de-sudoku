package fr.grunberg.resolveurdesudoku.structure;

import java.util.ArrayList;
import java.util.List;

/**
 * La grille du sudoku
 */
public class Grille {
	
public static final String GrilleFacile1 = "53__7____6__195____98____6_8___6___34__8_3__17___2___6_6____28____419__5____8__79";
	
private boolean debugValiditéGrille = false;
	
private int nombreCellulesX, nombreCellulesY;
private int nombreSousCellulesX, nombreSousCellulesY;
private int tailleSousCellulesX, tailleSousCellulesY;
private int nombreSymboles;

private Case symboles[][];
	
public Grille (){
	this(9, 9, 3, 3, 9);
}

public Grille(int nombreCellulesX, int nombreCellulesY, int nombreSousCellulesX, int nombreSousCellulesY, int nombreSymboles) {
	this.nombreCellulesX = nombreCellulesX;
	this.nombreCellulesY = nombreCellulesY;
	this.nombreSousCellulesX = nombreSousCellulesX;
	this.nombreSousCellulesY = nombreSousCellulesY;
	this.nombreSymboles = nombreSymboles;
	
	tailleSousCellulesX = nombreCellulesX/nombreSousCellulesX;
	tailleSousCellulesY = nombreCellulesY/nombreSousCellulesY;
	
	symboles = new Case[nombreCellulesX][nombreCellulesY];
	for(int x = 0; x < nombreCellulesX; x++)
		for(int y = 0; y < nombreCellulesY; y++) {
			symboles[x][y] = new Case();
		}
}

/**
 * Initialise la grille avec la méthode complète
 * @param string
 */
public void initialiserMethodeComplète(String chaîneImport) {
	String[] symbolesImport = chaîneImport.split("");
	for(int y = 0; y < nombreCellulesY; y++)
		for(int x = 0; x < nombreCellulesX; x++) {
			symboles[x][y].setSymbole(symbolesImport[x + y*nombreCellulesX]);
			if(symboles[x][y].aSymbole())
				symboles[x][y].setSymboleOriginel(true);
		}
}

/**
 * Initialise la grille avec la méthode complète
 * @param string
 */
public void initialiserRésultatAttendu(String chaîneImport) {
	String[] symbolesImport = chaîneImport.split("");
	for(int y = 0; y < nombreCellulesY; y++)
		for(int x = 0; x < nombreCellulesX; x++)
			symboles[x][y].setSymboleAttendu(symbolesImport[x + y*nombreCellulesX]);
}

/**
 * Initialise une case
 * @param x
 * @param y
 * @param symbole
 */
public void initialiserCase(int x, int y, Symbole symbole) {
	symboles[x][y].setSymbole(symbole);
	symboles[x][y].setSymboleOriginel(true);
}

/**
 * La grille est-elle valide ?
 * @return
 */
public boolean isConstructionValide() {
	if(nombreCellulesX % nombreSousCellulesX != 0)
		return false;
	if(nombreCellulesY % nombreSousCellulesY != 0)
		return false;
	if((nombreCellulesX/nombreSousCellulesX) * (nombreCellulesY/nombreSousCellulesY) != nombreSymboles)
		return false;
	return true;
}

/**
 * Les symboles de la grille sont-ils valides ?
 * @return
 */
public boolean isValide() {
	// Vérification de chaque ligne
	for(int x = 0; x < nombreCellulesX; x++) {
		List<Symbole> symbolesDéjàTrouvés = new ArrayList<Symbole>();
		for(int y = 0; y < nombreCellulesY; y++) {
			if(symboles[x][y].aSymbole()) {
				if(symbolesDéjàTrouvés.contains((symboles[x][y].getSymbole()))) {
					if(debugValiditéGrille)
						System.err.println("Grille invalide : case " + x + "-" + y + " (symbole " + symboles[x][y].getSymbole() + ") déjà présente dans sa ligne.");
					return false;
				}
				symbolesDéjàTrouvés.add(symboles[x][y].getSymbole());
			}
		}
	}
	
	// Vérification de chaque colonne
	for(int y = 0; y < nombreCellulesY; y++) {
		List<Symbole> symbolesDéjàTrouvés = new ArrayList<Symbole>();
		for(int x = 0; x < nombreCellulesX; x++) {
			if(symboles[x][y].aSymbole()) {
				if(symbolesDéjàTrouvés.contains(symboles[x][y].getSymbole())) {
					if(debugValiditéGrille)
						System.err.println("Grille invalide : case " + x + "-" + y + " (symbole " + symboles[x][y].getSymbole() + ") déjà présente dans sa colonne.");
					return false;
				}
				symbolesDéjàTrouvés.add(symboles[x][y].getSymbole());
			}
		}
	}
	
	// Vérification de chaque sous-grille
	for(int scx = 0; scx < nombreSousCellulesX; scx++)
		for(int scy = 0; scy < nombreSousCellulesY; scy++) {
			List<Symbole> symbolesDéjàTrouvés = new ArrayList<Symbole>();
			for(int x = scx*tailleSousCellulesX; x < (scx+1)*tailleSousCellulesX ; x++)
				for(int y = scy*tailleSousCellulesY; y < (scy+1)*tailleSousCellulesY ; y++) {
					if(symboles[x][y].aSymbole()) {
						if(symbolesDéjàTrouvés.contains((symboles[x][y].getSymbole()))) {
							if(debugValiditéGrille)
								System.err.println("Grille invalide : case " + x + "-" + y + " (symbole " + symboles[x][y].getSymbole() + ") déjà présente dans sa cellule.");
							return false;
						}
						symbolesDéjàTrouvés.add(symboles[x][y].getSymbole());
					}
					
				}
		}
	
	return true;
}

/**
 * La grille est-elle complètement remplie ?
 * @return
 */
public boolean isComplet() {
	for(int x = 0; x < nombreCellulesX; x++)
		for(int y = 0; y < nombreCellulesY; y++)
			if(!symboles[x][y].aSymbole())
				return false;
	return true;
}

public int getNombreCellulesX() {
	return nombreCellulesX;
}

public int getNombreCellulesY() {
	return nombreCellulesY;
}

public int getNombreSousCellulesX() {
	return nombreSousCellulesX;
}

public int getNombreSousCellulesY() {
	return nombreSousCellulesY;
}

public int getTailleSousCellulesX() {
	return tailleSousCellulesX;
}

public int getTailleSousCellulesY() {
	return tailleSousCellulesY;
}

public int getNombreSymboles() {
	return nombreSymboles;
}

/**
 * Restitue la grille (initiale) sous forme d'une chaîne prête à l'import
 */
public String getGrilleExportMethodeComplète() {
	String export = "";
	for(int y = 0; y < nombreCellulesY; y++)
		for(int x = 0; x < nombreCellulesX; x++)
			export = export + symboles[x][y].getSymboleExport();
	return export;
}

/**
 * Affiche la grille vers la sortie standard
 */
public void afficherGrille() {
	for(int x = 0; x < nombreCellulesX+2; x++)
		System.out.print("-");
	System.out.println();
	for(int y = 0; y < nombreCellulesY; y++) {
		System.out.print("|");
		for(int x = 0; x < nombreCellulesX; x++) {
			System.out.print(symboles[x][y].toString());
		}
		System.out.println("|");
	}
	for(int x = 0; x < nombreCellulesX+2; x++)
		System.out.print("-");
	System.out.println();
}

/**
 * Renvoie la prochaine cellule qui n'est pas remplie
 * @return coordonnées de la cellule en x-y
 */
public int[] getProchaineCelluleVide() {
	for(int y = 0; y < nombreCellulesY; y++)
		for(int x = 0; x < nombreCellulesX; x++)
			if(!symboles[x][y].aSymbole())
				return new int[] {x, y};
	return null;
}

public void écrireSymbole(int x, int y, Symbole symbole) {
	symboles[x][y].setSymbole(symbole);
}

public void effacerSymbole(int x, int y) {
	symboles[x][y].setSymbole(Case.SymboleVide);
}

/**
 * Renvoie la dernière cellule où un symbole a été écrit (pas de symbole d'initialisation)
 * @return
 */
public int[] getDernièreCelluleEcrite() {
	for(int y = nombreCellulesY-1; y >= 0; y--)
		for(int x = nombreCellulesX-1; x >=0 ; x--)
			if(symboles[x][y].aSymbole() && !symboles[x][y].isSymboleOriginel())
				return new int[] {x, y};
	return null;
}

/**
 * Renvoie le symbole présent sur cette case
 * @param x
 * @param y
 * @return
 */
public Symbole getSymbole(int x, int y) {
	return symboles[x][y].getSymbole();
}

/**
 * Renvoie le symbole suivant
 * @param précédentSymbole
 * @return
 */
public Symbole getProchainSymbole(Symbole précédentSymbole) {
	if(précédentSymbole.getSymbole() < 0)
		return new Symbole((byte)1);
	if(précédentSymbole.getSymbole() >= nombreSymboles)
		throw new ExceptionSymboleInvalide("Le symbole " + précédentSymbole + " est le dernier de la liste, impossible de lui trouver un successeur.");
	return new Symbole((byte)(précédentSymbole.getSymbole()+1));
}

/**
 * Renvoie la liste des symboles autorisés
 * @return
 */
public List<Symbole> getListeSymboles() {
	List<Symbole> liste = new ArrayList<Symbole>();
	for(int s = 1; s <= nombreSymboles; s++)
		liste.add(new Symbole((byte) s));
	return liste;
}

/**
 * Ajoute une liste de symboles comme possibilités
 * @param x
 * @param y
 * @param listeSymboles
 */
public void addSymbolesPossibles(int x, int y, List<Symbole> listeSymboles) {
	symboles[x][y].addSymbolesPossibles(listeSymboles);
	}
/**
 * Retire un symbole comme possibilité
 * @param x
 * @param y
 * @param symbole
 */
public void retireSymbolePossible(int x, int y, Symbole symbole) {
	symboles[x][y].retireSymbolePossible(symbole);
	}

/**
 * Renvoie la liste des symboles possibles
 * @return
 */
public List<Symbole> getListeSymbolesPossibles(int x, int y) {
	return symboles[x][y].getListeSymbolesPossibles();
}
/**
 * Renvoie les coordonnées min max, x et y, de la sous grille à laquelle appartient cette cellule
 * @param x
 * @param y
 * @return
 */
public int[] getCoordonnéesSousGrille(int x, int y) {
	int[] coordonnées = new int[4];
	coordonnées[0] = ((int) (x / tailleSousCellulesX)) * tailleSousCellulesX;
	coordonnées[1] = ((int) (y / tailleSousCellulesY)) * tailleSousCellulesY;
	coordonnées[2] = coordonnées[0] + tailleSousCellulesX - 1;
	coordonnées[3] = coordonnées[1] + tailleSousCellulesY - 1;
	return coordonnées;
}

}
