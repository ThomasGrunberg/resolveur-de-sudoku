package fr.grunberg.resolveurdesudoku.résolveur;

import fr.grunberg.resolveurdesudoku.structure.Grille;

import fr.grunberg.resolveurdesudoku.structure.Case;

public class RésolveurIntuitif extends Résolveur {
	private boolean debugEtapeParEtape = false;

	public RésolveurIntuitif(Grille grilleARésoudre) {
		super(grilleARésoudre);
	}

	@Override
	public boolean Résoudre() {
		démarrerRésolution();

		// Pose la liste complète de tous les symboles autorisés, dans chaque case vide
		for(int y = 0; y < grilleARésoudre.getNombreCellulesY(); y++)
			for(int x = 0; x < grilleARésoudre.getNombreCellulesX(); x++)
				if(grilleARésoudre.getSymbole(x, y) == null || Case.SymboleVide.equals(grilleARésoudre.getSymbole(x, y)))
					grilleARésoudre.addSymbolesPossibles(x, y, grilleARésoudre.getListeSymboles());
		for(int y = 0; y < grilleARésoudre.getNombreCellulesY(); y++)
			for(int x = 0; x < grilleARésoudre.getNombreCellulesX(); x++)
				retirerSymbolesImpossiblesACauseDeCetteCase(x,y);
		
		// Commence les déductions
		boolean déductionFaite = true;
		while(déductionFaite) {
			déductionFaite = false;
			int déductionsFaites = 0;
			if(debugEtapeParEtape)
				grilleARésoudre.afficherGrille();
			// Une passe de déduction
			for(int y = 0; y < grilleARésoudre.getNombreCellulesY(); y++)
				for(int x = 0; x < grilleARésoudre.getNombreCellulesX(); x++)
					if(grilleARésoudre.getSymbole(x, y) == null && grilleARésoudre.getListeSymboles() != null && grilleARésoudre.getListeSymbolesPossibles(x, y).size() == 1) {
						grilleARésoudre.écrireSymbole(x, y, grilleARésoudre.getListeSymbolesPossibles(x, y).get(0));
						retirerSymbolesImpossiblesACauseDeCetteCase(x,y);
						déductionFaite = true;
						déductionsFaites++;
					}
			if(debugEtapeParEtape)
				System.out.println(déductionsFaites + " déductions.");
		}
		
		return terminerRésolution();
	}

	/**
	 * En s'appuyant sur le symble présent dans la case passée en paramètre,
	 * retire comme symboles possibles ce symboles dans la colonne/ligne/sous-cellule correspondant
	 * @param x
	 * @param y
	 */
	private void retirerSymbolesImpossiblesACauseDeCetteCase(int x, int y) {
		if(grilleARésoudre.getSymbole(x, y) == null)
			return;
		if(Case.SymboleVide.equals(grilleARésoudre.getSymbole(x, y)))
			return;

		// Retire les symboles de la ligne courante comme possible pour les autres cases
		for(int xc = 0; xc < grilleARésoudre.getNombreCellulesX(); xc++)
			if(xc != x)
				grilleARésoudre.retireSymbolePossible(xc, y, grilleARésoudre.getSymbole(x, y));
		// Retire les symboles de la colonne courante comme possible pour les autres cases
		for(int yc = 0; yc < grilleARésoudre.getNombreCellulesX(); yc++)
			if(yc != y)
				grilleARésoudre.retireSymbolePossible(x, yc, grilleARésoudre.getSymbole(x, y));
		// Retire les symboles de la sous-grille comme possible pour les autres cases
		int coordonnéesSousGrille[] = grilleARésoudre.getCoordonnéesSousGrille(x, y);
		for(int scx = coordonnéesSousGrille[0]; scx <= coordonnéesSousGrille[2]; scx++)
			for(int scy = coordonnéesSousGrille[1]; scy <= coordonnéesSousGrille[3]; scy++)
				grilleARésoudre.retireSymbolePossible(scx, scy, grilleARésoudre.getSymbole(x, y));

	}
}
