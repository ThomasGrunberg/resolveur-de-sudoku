 - Le logiciel ici présent est protégé par le droit d'auteur. Son utilisation est autorisée dans le contexte du test de ce exercice pour le bénéfice de son auteur. Sa réutilisation, modification ou diffusion en dehors de ce contexte est interdite.
Ce logicel est fourni sans garantie d'aucune sorte. L'auteur ne saurait en aucune circonstance être tenu responsable de dommages causés par ce logiciel ou son utilisation.

 - This source code is protected under private licensing. Its use is authorized in the context of testing of this exercise, for the benefit of the author. Its reuse, modification or spreading is not allowed outside of this context.
 The software is provided "as is", without warranty of any kind. In no event shall the authors or copyright holders be liable for any claim, damages or liability arising from the software or the use of the software.
