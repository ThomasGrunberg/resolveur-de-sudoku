package fr.grunberg.resolveurdesudoku.structure;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Teste la validité des méthodes et des constructeurs
 */
public class StructureTest {
	/**
	 * Vérifier que les constructions de grille sont correctes
	 */
	@Test
	public void testerGrille() {
		Grille grilleARésoudre = new Grille();
		assertTrue(grilleARésoudre.isConstructionValide());
		assertTrue(grilleARésoudre.isValide());
		assertFalse(grilleARésoudre.isComplet());

		grilleARésoudre = new Grille(12, 12, 4, 4, 9);
		assertTrue(grilleARésoudre.isConstructionValide());
		assertTrue(grilleARésoudre.isValide());
		assertFalse(grilleARésoudre.isComplet());

		grilleARésoudre = new Grille(16, 16, 4, 4, 16);
		assertTrue(grilleARésoudre.isConstructionValide());
		assertTrue(grilleARésoudre.isValide());
		assertFalse(grilleARésoudre.isComplet());

		grilleARésoudre = new Grille(12, 12, 4, 4, 12);
		assertFalse(grilleARésoudre.isConstructionValide());
		assertTrue(grilleARésoudre.isValide());
		assertFalse(grilleARésoudre.isComplet());
		}
	
	/**
	 * Vérifier que le remplissage unitaire de grille fait son office
	 */
	@Test
	public void testerRemplissageUnitaireGrille() {
		Grille grilleARésoudre = new Grille();
		grilleARésoudre.initialiserCase(0, 0, new Symbole((byte) 5));
		grilleARésoudre.initialiserCase(1, 0, new Symbole((byte) 3));
		grilleARésoudre.initialiserCase(4, 0, new Symbole((byte) 7));
		grilleARésoudre.initialiserCase(0, 1, new Symbole((byte) 6));
		grilleARésoudre.initialiserCase(3, 1, new Symbole((byte) 1));
		grilleARésoudre.initialiserCase(4, 1, new Symbole((byte) 9));
		grilleARésoudre.initialiserCase(5, 1, new Symbole((byte) 5));
		grilleARésoudre.initialiserCase(1, 2, new Symbole((byte) 9));
		grilleARésoudre.initialiserCase(2, 2, new Symbole((byte) 8));
		grilleARésoudre.initialiserCase(7, 2, new Symbole((byte) 6));
		grilleARésoudre.initialiserCase(0, 3, new Symbole((byte) 9));
		grilleARésoudre.initialiserCase(4, 3, new Symbole((byte) 6));
		grilleARésoudre.initialiserCase(8, 3, new Symbole((byte) 3));
		grilleARésoudre.initialiserCase(0, 4, new Symbole((byte) 4));
		grilleARésoudre.initialiserCase(3, 4, new Symbole((byte) 8));
		grilleARésoudre.initialiserCase(5, 4, new Symbole((byte) 3));
		grilleARésoudre.initialiserCase(8, 4, new Symbole((byte) 1));
		grilleARésoudre.initialiserCase(0, 5, new Symbole((byte) 7));
		grilleARésoudre.initialiserCase(4, 5, new Symbole((byte) 2));
		grilleARésoudre.initialiserCase(8, 5, new Symbole((byte) 6));
		grilleARésoudre.initialiserCase(1, 6, new Symbole((byte) 6));
		grilleARésoudre.initialiserCase(6, 6, new Symbole((byte) 2));
		grilleARésoudre.initialiserCase(7, 6, new Symbole((byte) 8));
		grilleARésoudre.initialiserCase(3, 7, new Symbole((byte) 4));
		grilleARésoudre.initialiserCase(4, 7, new Symbole((byte) 1));
		grilleARésoudre.initialiserCase(5, 7, new Symbole((byte) 9));
		grilleARésoudre.initialiserCase(8, 7, new Symbole((byte) 5));
		grilleARésoudre.initialiserCase(4, 8, new Symbole((byte) 8));
		grilleARésoudre.initialiserCase(7, 8, new Symbole((byte) 7));
		grilleARésoudre.initialiserCase(8, 8, new Symbole((byte) 9));
		//grilleARésoudre.afficherGrille();
		//System.out.println(grilleARésoudre.getGrilleExportMethodeComplète());
		assertTrue(grilleARésoudre.isConstructionValide());
		assertTrue(grilleARésoudre.isValide());
		assertFalse(grilleARésoudre.isComplet());
	}

	/**
	 * Vérifier que le remplissage en chaine de grille fait son office
	 */
	@Test
	public void testerRemplissageMéthodeComplèteGrille() {
		Grille grilleARésoudre = new Grille();
		grilleARésoudre.initialiserMethodeComplète(Grille.GrilleFacile1);
		//grilleARésoudre.afficherGrille();
		//System.out.println(grilleARésoudre.getGrilleExportMethodeComplète());
		assertTrue(grilleARésoudre.isConstructionValide());
		assertTrue(grilleARésoudre.isValide());
		assertFalse(grilleARésoudre.isComplet());
	}

	/**
	 * Vérifier que le remplissage en chaine de grille fait son office
	 */
	@Test
	public void testerRemplissageMéthodeComplèteIncorrectGrille() {
		Grille grilleARésoudre = new Grille();
		grilleARésoudre.initialiserMethodeComplète("535_7____6__195____98____6_8___6___34__8_3__17___2___6_6____28____419__5____8__79");
		//grilleARésoudre.afficherGrille();
		//System.out.println(grilleARésoudre.getGrilleExportMethodeComplète());
		assertTrue(grilleARésoudre.isConstructionValide());
		assertFalse(grilleARésoudre.isValide());
		assertFalse(grilleARésoudre.isComplet());
	}
	/**
	 * Vérifier que le calcul des coordonnées de grille est correct
	 */
	@Test
	public void testerCalculCoordonnéesGrille() {
		Grille grilleARésoudre = new Grille();
		//grilleARésoudre.initialiserMethodeComplète(Grille.GrilleFacile1);
		int[] coordonnées = grilleARésoudre.getCoordonnéesSousGrille(0, 0);
		assertEquals(0, coordonnées[0]);
		assertEquals(0, coordonnées[1]);
		assertEquals(2, coordonnées[2]);
		assertEquals(2, coordonnées[3]);
		coordonnées = grilleARésoudre.getCoordonnéesSousGrille(3, 4);
		assertEquals(3, coordonnées[0]);
		assertEquals(3, coordonnées[1]);
		assertEquals(5, coordonnées[2]);
		assertEquals(5, coordonnées[3]);
		coordonnées = grilleARésoudre.getCoordonnéesSousGrille(1, 6);
		assertEquals(0, coordonnées[0]);
		assertEquals(6, coordonnées[1]);
		assertEquals(2, coordonnées[2]);
		assertEquals(8, coordonnées[3]);
		coordonnées = grilleARésoudre.getCoordonnéesSousGrille(8, 2);
		assertEquals(6, coordonnées[0]);
		assertEquals(0, coordonnées[1]);
		assertEquals(8, coordonnées[2]);
		assertEquals(2, coordonnées[3]);
	}
}
