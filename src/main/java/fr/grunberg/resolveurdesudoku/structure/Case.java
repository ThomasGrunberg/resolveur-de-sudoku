package fr.grunberg.resolveurdesudoku.structure;

import java.util.ArrayList;
import java.util.List;

/**
 * Une case de la grille de sudoku, avec les attributs utiles
 */
public class Case {
public static final Symbole SymboleVide = new Symbole((byte)-1);
	
private Symbole symbole, symboleAttendu;
private boolean symboleOriginel;
private List<Symbole> listeSymbolesPossibles;
	
	public Case() {
		symbole = SymboleVide;
		symboleOriginel = false;
	}

	public Symbole getSymbole() {
		return symbole;
	}

	public boolean aSymbole() {
		return (symbole != null && !symbole.equals(SymboleVide));
	}
	
	public void setSymbole(Symbole symbole) {
		if(symboleOriginel)
			throw new ExceptionSymboleImmuable("Changement d'un symbole d'origine.");
		this.symbole = symbole;
	}

	public void setSymbole(String symboleString) {
		try {
			setSymbole(new Symbole(symboleString));
		}
		catch(NumberFormatException e) {
			symbole = null;
		}
	}

	public boolean isSymboleOriginel() {
		return symboleOriginel;
	}

	public void setSymboleOriginel(boolean symboleOriginel) {
		this.symboleOriginel = symboleOriginel;
		if(symboleOriginel)
			symboleAttendu = symbole;
			
	}

	/**
	 * Affiche le symbole, dans un format affichable
	 * @return
	 */
	@Override
	public String toString() {
		if(symbole != null && !SymboleVide.equals(symbole))
			return symbole.toString();
		if(listeSymbolesPossibles == null || listeSymbolesPossibles.size() == 0)
			return " ";
		switch(listeSymbolesPossibles.size()) {
			case 1 : return ".";
			case 2 : return ":";
			case 3 : return ";";
			case 4 : return "!";
			case 5 : return "-";
			case 6 : return "~";
			case 7 : return "=";
			case 8 : return "@";
			case 9 : return "?";
		}
		return " ";
	}

	/**
	 * Récupère le symbole, avec un caractère si la valeur est inconnue
	 * @return
	 */
	public String getSymboleExport() {
		if(symbole != null && symboleOriginel)
			return symbole.toString();
		return "_";
	}

	public Symbole getSymboleAttendu() {
		return symboleAttendu;
	}

	public void setSymboleAttendu(Symbole symboleAttendu) {
		this.symboleAttendu = symboleAttendu;
	}

	public void setSymboleAttendu(String symboleString) {
		try {
			setSymboleAttendu(new Symbole(symboleString));
		}
		catch(NumberFormatException e) {}
	}

	public void addSymbolesPossibles(List<Symbole> listeSymboles) {
		if(listeSymbolesPossibles == null)
			listeSymbolesPossibles = listeSymboles;
		else
			listeSymbolesPossibles.addAll(listeSymboles);
	}

	public void addSymbolePossible(Symbole symbole) {
		if(listeSymbolesPossibles == null)
			listeSymbolesPossibles = new ArrayList<Symbole>();
		listeSymbolesPossibles.add(symbole);
	}
	
	public void retireSymbolePossible(Symbole symbole) {
		if(listeSymbolesPossibles == null)
			return;
		listeSymbolesPossibles.remove(symbole);
	}

	public List<Symbole> getListeSymbolesPossibles() {
		return listeSymbolesPossibles;
	}
}
