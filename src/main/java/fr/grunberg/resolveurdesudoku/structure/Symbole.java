package fr.grunberg.resolveurdesudoku.structure;

/**
 * Un symbole numérique sur la grille de sudoku
 */
public class Symbole {
	private byte symbole;
	
	public Symbole(byte symbole) {
		this.symbole = symbole;
	}
	
	public Symbole(String symbole) {
		this.symbole = new Byte(symbole);
	}

	public byte getSymbole() {
		return symbole;
	}
	
	@Override
	public String toString() {
		return Byte.toString(symbole);
	}
	
	@Override
	public boolean equals(Object s) {
		if(s == null)
			return false;
		if(!(s instanceof Symbole))
			return false;
		return ((Symbole)s).getSymbole() == this.getSymbole();
	}
}
